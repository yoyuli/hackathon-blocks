const awsIot = require('aws-iot-device-sdk');
const util = require('util');

//
// Replace the values of '<YourUniqueClientIdentifier>' and '<YourCustomEndpoint>'
// with a unique client identifier and custom host endpoint provided in AWS IoT.
// NOTE: client identifiers must be unique within your AWS account; if a client attempts 
// to connect with a client identifier which is already in use, the existing 
// connection will be terminated.
//

var IOT=function(iotButtonListener){
    this.device = awsIot.device({
        keyPath: 'certificates/21fa752991-private.pem.key',
        certPath: 'certificates/21fa752991-certificate.pem.crt',
        caPath: 'certificates/ca.pem',
        clientId: 'yoyusmacbook',
        host: 'a2bs5cbf9ygnps.iot.eu-west-1.amazonaws.com'
    });
    this.device.subscribe('iotbutton/G030PT027097Q2CD');

    this.device
    .on('connect', function() {
        console.log('connect');
    });

    this.device
    .on('close', function() {
        console.log('close');
    });
    
    this.device
    .on('reconnect', function() {
        console.log('reconnect');
    });
    
    this.device
    .on('offline', function() {
        console.log('offline');
    });
    
    this.device
    .on('error', function(error) {
        console.log('error', error);
    });
    
    this.device
    .on('message', function(topic, payload) {
        console.log('message', topic, payload.toString());
        iotButtonListener.emit('iotbutton',payload.toString());
    });

}

module.exports=IOT;