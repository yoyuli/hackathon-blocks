const express = require('express');
const async = require('async')
const EventEmitter = require('events');
const IOT=require('./thing.js');
const Photo=require('./photo.js');
const Service=require('./service.js');

// Serving static server
var app = express();
app.use('/reveal',express.static('reveal', {'index': 'index.html'}))
app.listen(3000,()=>console.log('App listening on port 3000'));

// Serving socket server
var socketServer = require('http').Server(app);
var socketIO=require('socket.io');

var io = socketIO(socketServer,{
    transports:'websocket'
});


io.on('connection', function(socket){
    // console.log('connected')
    global.socketio=socket;
    global.socketio.on('slidechanged',(index)=>{
        console.log(index);
        global.index=index;
    })
});

socketServer.listen(3001,()=>console.log('Socket.io listening on port 3001'));

// Listening to IoT Button events
const iotButtonListener = new EventEmitter();
const iot=new IOT(iotButtonListener);
iotButtonListener.on('iotbutton',(data)=>{
    console.log('iotButtonListener event:'+data);
    run('test',(err,data)=>{
        if(err){
            console.error(err);
        }else{
            console.log('Response from Lambda'+data);
        }
    });
});

var constructLambdaPayload=(fileName,callback)=>{
    callback(null,{
        level: levelLookUp(global.index),
        key: fileName
    });
};

var levelLookUp=(index)=>{
    var levels={
        3:'1',
        4:'2'
    }
    try{
        var level=levels[index.h];
    }catch(e){
        console.error('Level out of boundary');
        var level=null;
    }
    return level;
   
};

var run=(fileName,callback)=>{
    var photo=new Photo(fileName);    // filename
    var service=new Service();
    var runner= async.seq(
                    photo.capture.bind(photo),
                    service.upload2S3.bind(service),
                    constructLambdaPayload,
                    service.invokeLambda.bind(service));
    runner(callback);
}

// Some REST API
app.get('/test', function (req, res) {
    console.log('test event');
    console.log('forward to socket');
    res.send('done');
    global.socketio.emit('news', { hello: 'world' });
});

app.get('/capture',function(req,res){
    run('test',(err,data)=>{
        if(err){
            console.error(err);
        }else{
            res.send(data);
        }
    });
});

app.get('/api/slide/:h/:v', function (req, res) {
    try{
        var data={
            h:req.params.h,
            v:req.params.v
        };
        global.socketio.emit('reveal',{slide:data});
        res.send('Sent reveal slide event');
    }catch(error){
        console.error(error);
        res.send('error');
    }
});

