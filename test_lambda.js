//set up variables
var AWS = require('aws-sdk');
var lambda = new AWS.Lambda({
    region:'eu-west-1'
});
var params = {
  FunctionName: 'two-blocks-away-lambda-dev-hello', 
  Payload: '{"level": "1","key": "level2.jpg"}'
};

//invoke the lambda
lambda.invoke(params, function(err, data) {
  if (err) console.log(err, err.stack); // an error occurred
  else     console.log(data);           // successful response
});
