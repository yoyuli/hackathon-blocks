# two-blocks-away
The descriptionof the project can be found [here](https://cloudreach.jira.com/wiki/spaces/cre8/pages/623083556/Two+Blocks+Away)

## File structure
- `/reveal/`: the presentation layer using reveal.js framework
- `/index.js`: the starting point of the application
- `/photo.js`: the camera module to take pictures with Raspberry Pi
- `/photo/`: where images captured by the camera go
- `/algorithm.js`: the algorithm used to compare AWS rekognition results to correct answers
- `/thing.js`: sample code to publish/subscribe to an AWS IoT topic

## How to
To test the application, follow the following steps:
1. Clone the repository
2. Install the node.js packages by running `npm install` 
3. Run the test application: `node index.js`
4. The front-end app will listen on port 3000 - go to the browser and navigate to the [http://localhost:3000/reveal/index.html](http://localhost:3000/reveal/index.html)

## Todos

1.Create a *Thing*(Raspberry Pi) in AWS IoT, and give the *Thing* permissions to:

    - Upload objects to an S3 bucket
    
    - Pub/Sub from an IoT topic
    
    - Call a Lambda function through API 
    
2.Create an IoT rule - whenever the IoT button is pressed, publish a message in the IoT topic 

3.Complete the Raspberry Pi code in order to:

    - Authenticate Raspberry Pi using AWS IoT SDK - **Done** (`thing.js`)
    
    - Upon receiving a "button pressed" message from the IoT topic, take a picture using the camera module **Done** (`photo.js`)
    
    - Upload the picture to the S3 bucket
    
    - Trigger a Lambda function
    
4.Complete the Lambda function:
    
    - Take the S3 object key from the request and call AWS Rekognition
    
    - Upon receiving the results from Rekognition, compare the results using the algorithm - **Done**(`algorithm.js`)
    
    - Return a response and tell the client whether the solution is correct or not
    
5.The front-end app acts to the response 
    
    - Socket.io communication between the node.js app and the front-end app - **Done** 
    
    - An API to control the front-end app, e.g. "correct"/"not correct", progressing to next level - **Done** 
    
    - Based on the Lambda response, trigger API correspondingly 
    
6.More level design