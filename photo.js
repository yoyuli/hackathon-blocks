var RaspiCam = require("raspicam");

var Photo=function(fileName){
    this.output="./photo/"+fileName+".jpg";
    this.camera = new RaspiCam({
        mode: "photo",
        output: this.output,
        encoding: "jpg",
        timeout: 0, // take the picture immediately
        nopreview:true
    });
}

Photo.prototype.capture=function(callback){
    var _this=this;
    this.camera.on("start", function( err, timestamp ){
        console.log("photo started at " + timestamp );
    });
    
    this.camera.on("read", function( err, timestamp, fileName ){
        console.log("photo image captured with filename: " + fileName );
        _this.camera.stop();
        callback(null,'photo/'+fileName);
    });
    
    this.camera.on("exit", function( timestamp ){
        console.log("photo child process has exited at " + timestamp );
    });

    this.camera.start();
}

module.exports=Photo;

