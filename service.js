var LAMBDA_FUNCTION='two-blocks-away-lambda-dev-hello';
var S3_BUCKET='two-blocks-away';
var AWS_REGION='eu-west-1';
var AWS = require('aws-sdk');

var Service=function(){

};

Service.prototype.invokeLambda=(payload,callback)=>{
    var Lambda = new AWS.Lambda({
        region:AWS_REGION
    });
    var params = {
        FunctionName: LAMBDA_FUNCTION, 
        Payload: JSON.stringify(payload)
        //example: '{"level": "1","key": "level2.jpg"}'
    };
    Lambda.invoke(params,callback);
};

Service.prototype.upload2S3=(fileName,callback)=>{
    var fs =  require('fs');
    var S3=new AWS.S3({
        region:AWS_REGION
    })
    fs.readFile(fileName, function (err, data) {
        if (err) { throw err; }
        var params = {
            Bucket: S3_BUCKET,
            Key: fileName, 
            Body: data 
        };
        S3.putObject(params, (err,data)=>{
            callback(err,fileName); //  callback fileName instead of data
        });
    });
};

module.exports=Service;
