
/*
var JUSTIFICATION=[
    {X:0,Y:0},
    {X:0,Y:0},
    {X:0,Y:0},
    {X:0,Y:0}
]
*/

var MATCH_RADIUS=0.3;

/*
// sampleResponse:
// TextDetections: Array
// individual objects
// - Type: we need "WORD"
// - DetectedText 
// - Geometry
//      - BoundingBox
//      - Polygon: Point array
//          -   Point objects {X:...,Y:...}
*/

var answers=[
    {
        name:'EC2',
        position:{X:0.8,Y:0.5}
    },
    {
        name:'RDS',
        position:{X:0.5,Y:0.5}
    }
]

var extractWords=function(TextDetections){
    var words={};
    TextDetections.map((textObject)=>{
        if(textObject.Type=='WORD'){
            words[textObject.DetectedText]=getPolygonCentre(textObject.Geometry.Polygon)
        }
    });
    return words;
    /*
        words={
            'WORD1':{X:...,Y:...},
            'WORD2':{X:...,Y:...}
        }
    */
}

var getPolygonCentre=function(polygon){
    var x=((polygon[0].X+polygon[3].X)/2+(polygon[1].X+polygon[2].X)/2)/2;
    var y=((polygon[0].Y+polygon[1].Y)/2+(polygon[2].Y+polygon[3].Y)/2)/2;
    return {
        X:x,
        Y:y
    }
}

var getDistance=function(dot1,dot2){
    var d1=Math.abs(dot1.X-dot2.X);
    var d2=Math.abs(dot1.Y-dot2.Y);
    var s1=Math.pow(d1,2);
    var s2=Math.pow(d2,2);
    return Math.sqrt(s1+s2)
}

var matchAnswer=function(answers,words){
    for(answer of answers){
        console.log('Searching for '+answer.name);
        if(words[answer.name]){
            console.log('Found '+answer.name);
            var distance=getDistance(words[answer.name],answer.position);
            console.log('Distance: '+distance);
            if(distance<MATCH_RADIUS) {
                console.log('Matched '+answer.name)
                
            }else{
                console.log('Incorrect position: '+answer.name)
                return false;
            }
        }else{
            console.log('Not found: '+answer.name)
            return false;
        }
    }
    console.log('All matched');
    return true;
}

var sampleResponse={
    "TextDetections": [
        {
            "Confidence": 99.47185516357422,
            "DetectedText": "EC2",
            "Geometry": {
                "BoundingBox": {
                    "Height": 0.3739858865737915,
                    "Left": 0.48920923471450806,
                    "Top": 0.5900818109512329,
                    "Width": 0.5097314119338989
                },
                "Polygon": [
                    {
                        "X": 0.48475268483161926,
                        "Y": 0.6823741793632507
                    },
                    {
                        "X": 0.9601503014564514,
                        "Y": 0.587857186794281
                    },
                    {
                        "X": 0.9847385287284851,
                        "Y": 0.8692590594291687
                    },
                    {
                        "X": 0.5093409419059753,
                        "Y": 0.9637760519981384
                    }
                ]
            },
            "Id": 10,
            "ParentId": 4,
            "Type": "WORD"
        },
        {
            "Confidence": 99.47185516357422,
            "DetectedText": "S3",
            "Geometry": {
                "BoundingBox": {
                    "Height": 0.3739858865737915,
                    "Left": 0.48920923471450806,
                    "Top": 0.5900818109512329,
                    "Width": 0.5097314119338989
                },
                "Polygon": [
                    {
                        "X": 0.48475268483161926,
                        "Y": 0.6823741793632507
                    },
                    {
                        "X": 0.9601503014564514,
                        "Y": 0.587857186794281
                    },
                    {
                        "X": 0.9847385287284851,
                        "Y": 0.8692590594291687
                    },
                    {
                        "X": 0.5093409419059753,
                        "Y": 0.9637760519981384
                    }
                ]
            },
            "Id": 10,
            "ParentId": 4,
            "Type": "WORD"
        }
    ]
}

if (sampleResponse.TextDetections && sampleResponse.TextDetections.length>0){
    var words= extractWords(sampleResponse.TextDetections)
    matchAnswer(answers,words);
}

